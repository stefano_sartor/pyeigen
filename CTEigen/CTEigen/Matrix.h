#ifndef CT_MATRIX_H
#define CT_MATRIX_H

#include "CTEigen.h"
#include <memory>
#include <iostream>


namespace CommonTools{
template <typename _Scalar, int _Rows, int _Cols> class Matrix;
}

namespace Eigen{
namespace internal {

template <typename _Scalar, int _Rows, int _Cols>
 struct traits<CommonTools::Matrix<_Scalar,_Rows,_Cols>> :
 public traits<Map<Eigen::Matrix<_Scalar,_Rows,_Cols>>>{

};


} //namespace internal
} // namespace Eigen


namespace CommonTools{


//TODO check Dynamic vs Fixed cols/ros assigment
template <typename _Scalar,int _Rows = Dynamic,int _Cols = Dynamic>
class Matrix:
    public Eigen::Map<Eigen::Matrix<_Scalar,Dynamic,Dynamic,Eigen::RowMajorBit> > {
    typedef  Eigen::Map<Eigen::Matrix<_Scalar,Dynamic,Dynamic,Eigen::RowMajorBit> > Base;

public:

    EIGEN_GENERIC_PUBLIC_INTERFACE(Matrix)

    using Base::operator=;

    template<class E>
    Matrix(const Eigen::MatrixBase<E>& other) :
    Base(new Scalar[_Rows*_Cols]),
    ptr_(Base::data(),_Rows,_Cols){
        *this = other;
	std::cout << "copy buffer constructor: " <<(void*) Base::data() << std::endl; // XXX DEBUG
        //FIXME check dimensions?
    }
    
    Matrix(Scalar* d) :
       Base(d)
    {}

 Matrix(int v) :
    Base(new Scalar[v],_Rows!=Dynamic?_Rows:v,_Cols!=Dynamic?_Cols:v),
    ptr_(Base::data()){
      static_assert(_Rows == Dynamic || _Cols == Dynamic,"matrix dimensions fixed by template arguments");
    }
    
    template <class Deleter>
    Matrix(Scalar* d, int v, Deleter md) :
    Base(d,_Rows!=Dynamic?_Rows:v,_Cols!=Dynamic?_Cols:v),
     ptr_(d,md){
      static_assert(_Rows == Dynamic || _Cols == Dynamic,"matrix dimensions fixed by template arguments");    
   }

    Matrix() :
    Base(new Scalar[_Rows*_Cols],_Rows,_Cols),
    ptr_(Base::data()){
      static_assert(_Rows != Dynamic || _Cols != Dynamic,"default constructor requires non Dynamic dimensions"); 
    }

    template <class Deleter>
    Matrix(Scalar* d, Deleter md) :
    Base(d,_Rows,_Cols),
     ptr_(d,md)
   {}

    /*    //XXX debug
    ~Matrix(){
        std::cout << "MMatrix destructor" << std::endl;
	if(ptr_)
	  std::cout <<"  buffer: "<< ptr_.get() << "," <<Base::data()<< "  count:" << ptr_.use_count() -1<< std::endl;
        else
            std::cout << "  buffer: "<< ptr_.get() << "," <<Base::data()<< "  count: not managed" << std::endl;
    }
    */
    Matrix& operator=(const Matrix& other){
      //std::cout << "operator= " << (void*) Base::data() <<"," <<(void*) other.data()<< std::endl; // XXX Debug
      new (static_cast<Base*>(this)) Base(const_cast<Matrix&>(other).data(),other.rows(),other.cols());
      ptr_ = other.ptr_;
      //std::cout << "after   = " << (void*) Base::data() <<"," <<(void*) other.data()<< std::endl; // XXX Debug
      return *this;
    }


    void resize(Index rows, Index cols) = delete;
    void resize(Index size) = delete;

protected:
    std::shared_ptr<Scalar> ptr_;
};


//TODO check Dynamic vs Fixed cols/ros assigment
template <typename _Scalar>
class Matrix<_Scalar, Dynamic, Dynamic>:
    public Eigen::Map<Eigen::Matrix<_Scalar,Dynamic,Dynamic,Eigen::RowMajorBit> > {

    typedef typename Eigen::Map<Eigen::Matrix<_Scalar,Dynamic,Dynamic,Eigen::RowMajorBit> > Base;

public:


    EIGEN_GENERIC_PUBLIC_INTERFACE(Matrix)

    using Base::operator=;

    template<class E>
    Matrix(const Eigen::MatrixBase<E>& other) :
    Base(new Scalar[other.rows()*other.cols()],other.rows(),other.cols()),
    ptr_(Base::data()){
      *this = other;
    }

    
    Matrix(Scalar* d, int rows, int cols) :
      Base(d,rows,cols)
    {}

    Matrix(int rows, int cols) :
        Base(new Scalar[rows*cols],rows,cols),
        ptr_(Base::data())
    {}


    template <class Deleter>
    Matrix(Scalar* d, int rows, int cols, Deleter del) :
     Base(d,rows,cols),
     ptr_(d,del)
   {}

     /*    //XXX debug 
    ~Matrix(){
        std::cout << "MMatrix["<< Base::rows() <<","<< Base::cols() <<"] destructor" << std::endl;
        if(ptr_)
	  std::cout <<"  buffer: "<< ptr_.get() << "," <<Base::data()<< "  count:" << ptr_.use_count() -1<< std::endl;
        else
            std::cout << "  buffer: "<< ptr_.get() << "," <<Base::data()<< "  count: not managed" << std::endl;
    }
*/
     
    Matrix& operator=(const Matrix& other){
      //std::cout << "operator= " << (void*) Base::data() <<"," <<(void*) other.data()<< std::endl; // XXX Debug
      new (static_cast<Base*>(this)) Base(const_cast<Matrix&>(other).data(),other.rows(),other.cols());
      ptr_ = other.ptr_;
      //std::cout << "after   = " << (void*) Base::data() <<"," <<(void*) other.data()<< std::endl; // XXX Debug
      return *this;
    }
    
    void resize(Index rows, Index cols) = delete;
    void resize(Index size) = delete;

protected:
    std::shared_ptr<Scalar> ptr_;
};

//template<typename Scalar> using Vector = Matrix<Scalar,Dynamic,1>;
//template<typename Scalar> using RowVector = Matrix<Scalar,1,Dynamic>;

// Workaround for the SWIG missing 'using' feature of C++11 (which is ignored)
template< typename Scalar>
class Vector: public Matrix<Scalar, Dynamic, 1> {
 public:
  using super = Matrix<Scalar, Dynamic, 1>;
  using super::super;

  ~Vector() {}

};

template<typename Scalar>
class RowVector: public Matrix<Scalar, 1, Dynamic> {
 public:
  using super = Matrix<Scalar, 1, Dynamic>;
  using super::super;

  ~RowVector() {}

};

}

#endif
