#ifndef CT_EIGEN_H
#define CT_EIGEN_H

#include <Eigen/Core>
#include <Eigen/Dense>

namespace CommonTools{
const int Dynamic = Eigen::Dynamic;

}// CommonTools

#endif
