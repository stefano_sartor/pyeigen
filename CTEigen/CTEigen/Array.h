#ifndef CT_ARRAY_H
#define CT_ARRAY_H

#include "CTEigen.h"
#include <memory>
#include <iostream>
#include <algorithm>


namespace CommonTools{
template <typename _Scalar, int _Rows, int _Cols> class Array;
}

namespace Eigen{
namespace internal {

template <typename _Scalar, int _Rows, int _Cols>
 struct traits<CommonTools::Array<_Scalar,_Rows,_Cols>> :
 public traits<Map<Eigen::Array<_Scalar,_Rows,_Cols>>>{

};


} //namespace internal
} // namespace Eigen


namespace CommonTools{
 
//TODO check Dynamic vs Fixed cols/ros assigment
template <typename _Scalar,int _Rows = Dynamic,int _Cols = Dynamic>
class Array:
    public Eigen::Map<Eigen::Array<_Scalar,_Rows,_Cols,Eigen::RowMajorBit> > {
    typedef  Eigen::Map<Eigen::Array<_Scalar,_Rows,_Cols,Eigen::RowMajorBit> > Base;

 public:
    
    EIGEN_GENERIC_PUBLIC_INTERFACE(Array)
    
    using Base::operator=;

    template<class E>
    Array(const Eigen::ArrayBase<E>& other) :
    Base(new Scalar[_Rows*_Cols]),
    ptr_(Base::data()){
      *this = other;
      //FIXME check dimensions?
    }
    
 Array(int v) :
    Base(new Scalar[v],v),
    ptr_(Base::data()){
      static_assert(_Rows == Dynamic || _Cols == Dynamic,"matrix dimensions fixed by template arguments");
    }
    
    Array(Scalar* d) :
       Base(d)
    {}

    Array() :
        Base(new Scalar[_Rows*_Cols]),
        ptr_(Base::data())
    {}
    
    template <class Deleter>
    Array(Scalar* d, int v, Deleter md) :
    Base(d,v),
     ptr_(d,md){
      static_assert(_Rows == Dynamic || _Cols == Dynamic,"matrix dimensions fixed by template arguments");    
   }

    template <class Deleter>
    Array(Scalar* d, Deleter md) :
     Base(d),
     ptr_(d,md)
   {}


    /*    //XXX debug 
    ~Array(){
        std::cout << "MArray destructor" << std::endl;
        if(ptr_)
            std::cout << "  buffer: " << ptr_.get() << "  count:" << ptr_.use_count() -1 << std::endl;
        else
            std::cout << "  buffer: " << ptr_.get() << "  count: not managed" << std::endl;
    }
*/

    Array& operator=(const Array& other){
      //std::cout << "operator= " << (void*) Base::data() <<"," <<(void*) other.data()<< std::endl; // XXX Debug
      new (static_cast<Base*>(this)) Base(const_cast<Array&>(other).data(),other.rows(),other.cols());
      ptr_ = other.ptr_;
      //std::cout << "after   = " << (void*) Base::data() <<"," <<(void*) other.data()<< std::endl; // XXX Debug
      return *this;
    }


    Array copy() const {
      Array acopy(this->rows(), this->cols());
      std::copy(this->data(), this->data()+this->rows()*this->cols(), acopy.data());
      return acopy;
    }

    void resize(Index rows, Index cols) = delete;
    void resize(Index size) = delete;

protected:
    std::shared_ptr<Scalar> ptr_;
};


//TODO check Dynamic vs Fixed cols/ros assigment
template <typename _Scalar>
class Array<_Scalar, Dynamic, Dynamic>:
    public Eigen::Map<Eigen::Array<_Scalar,Dynamic,Dynamic,Eigen::RowMajorBit> > {

    typedef typename Eigen::Map<Eigen::Array<_Scalar,Dynamic,Dynamic,Eigen::RowMajorBit> > Base;

public:


    EIGEN_GENERIC_PUBLIC_INTERFACE(Array)

    using Base::operator=;

    template<class E>
    Array(const Eigen::ArrayBase<E>& other) :
    Base(new Scalar[other.rows()*other.cols()],other.rows(),other.cols()),
    ptr_(Base::data()){
        *this = other;
    }

    Array(Scalar* d, int rows, int cols) :
      Base(d,rows,cols)
    {}

    Array(int rows, int cols) :
        Base(new Scalar[rows*cols],rows,cols),
        ptr_(Base::data())
    {}

    template <class Deleter>
    Array(Scalar* d, int rows, int cols, Deleter del) :
     Base(d,rows,cols),
     ptr_(d,del)
   {}

     /*    //XXX debug
    ~Array(){
        std::cout << "MArray["<< Base::rows() <<","<< Base::cols() <<"] destructor" << std::endl;
        if(ptr_)
            std::cout << "  buffer: " << ptr_.get() << "  count:" << ptr_.use_count() -1 << std::endl;
        else
            std::cout << "  buffer: " << ptr_.get() << "  count: not managed" << std::endl;
    }
     */


    Array& operator=(const Array& other){
      //std::cout << "operator= " << (void*) Base::data() <<"," <<(void*) other.data()<< std::endl; // XXX Debug
      new (static_cast<Base*>(this)) Base(const_cast<Array&>(other).data(),other.rows(),other.cols());
      ptr_ = other.ptr_;
      //std::cout << "after   = " << (void*) Base::data() <<"," <<(void*) other.data()<< std::endl; // XXX Debug
      return *this;
    }


    Array copy() const {
      Array acopy(this->rows(), this->cols());
      std::copy(this->data(), this->data()+this->rows()*this->cols(), acopy.data());
      return acopy;
    }

    void resize(Index rows, Index cols) = delete;
    void resize(Index size) = delete;

protected:
    std::shared_ptr<Scalar> ptr_;
};


}

#endif
