Build and Test
==============

This cython wrapping example is not integrated in the cmake building chain.
It's only pourpose it to test the power and flexibility of cython as a c++
wrapper.

In order to build and test the wrapper you need to:

 * install cython 0.22 (LODEEN 1.0 provides al older non compatible version)
 * from the src directory run the following commands:
   ** python setup.py build_ext --inplace
   ** python example.py 
