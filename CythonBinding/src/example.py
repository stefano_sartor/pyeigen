import numpy as np

import ceigen as e  # wrapping example module

# create a wrapped object containing a 4x4 image
a = e.DetectorImage(4,4)

# type mapping converts c++ CommonTools::Matrix to numpy ndarray 
b = a.get_image()

# so we can use numpy methods
b.fill(9)
    
print('ndarray b filled with 9...')
print(b)

# using mapped methods to set the matrix to zeroes
a.zero()

# memory buffers are shared, b now contains zeroes
print('ndarray b indirectly zeroed...')
print(b)

# create a 3x3 matrix of double
c = np.array([[1,2,3],[4,5,6],[7,8,9]],dtype='double')

# type mapping converts numpy ndarray to c++ CommonTools::Matrix
a.set_image(c)

print('a.image updated..')
print(a.get_image())
