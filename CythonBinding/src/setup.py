from distutils.core import setup, Extension
from Cython.Build import cythonize


extensions = [
    Extension('ceigen',
              include_dirs=["/usr/include/eigen3","../../CTEigen/CTEigen"],
              sources=['ceigen.pyx'],
              language="c++",
              extra_compile_args=['-std=c++11']
    )
]

setup(name='ceigen',
      ext_modules = cythonize(extensions)
)
