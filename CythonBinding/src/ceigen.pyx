import numpy as np
cimport numpy as np
from cython.operator cimport dereference

np.import_array()


cdef extern from "Matrix.h" namespace "CommonTools":
    cdef cppclass Matrix[T]:
        Matrix(int x, int y)
        
    cdef cppclass Array[T]:
        Array(int x, int y)

        
cdef extern from "cy_eigen.h" :
    cdef cppclass Converter[T]:
        object to_numpy(const Matrix[T]& m)
        Matrix[T]* to_matrix(object o)

        object to_numpy(const Array[T]& a)
        Array[T]* to_array(object o)


cdef extern from "Examples.h" :
    cdef cppclass __DetectorImage "DetectorImage":
            __DetectorImage(int rows, int cols)
            Matrix[double] get_image()
            void set_image(Matrix[double] m)
            double get_gain()
            void set_gain(double g)
            double get_offset()
            void set_offset(double o)
            void zero()
            void print_ptr()
    


cdef class DetectorImage:
    cdef __DetectorImage *_thisptr

    def __cinit__(self,int rows, int cols):
        self._thisptr = new __DetectorImage(rows,cols)

    def __dealloc__(self):
        if self._thisptr != NULL:
            del self._thisptr

            
    def set_image(self,np.ndarray[np.float64_t,ndim=2] a):
        cdef Converter[double] cv
        cdef Matrix[double]* m = cv.to_matrix(a)
        self._thisptr.set_image(dereference(m))
        del m

    def get_image(self):
        cdef Converter[double] cv
        return cv.to_numpy(self._thisptr.get_image())

    def get_gain(self):
        return self._thisptr.get_gain()

    def set_gain(self, double o):
        self._thisptr.set_gain(o)

    def get_offset(self):
        return self._thisptr.get_offset()

    def set_offset(self,double o):
        self._thisptr.set_offset(o)

    def zero(self):
        self._thisptr.zero()

    def print_ptr(self):
        self._thisptr.print_ptr()


