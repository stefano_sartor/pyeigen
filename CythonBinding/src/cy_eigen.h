#include <boost/python/class.hpp>
#include <boost/python/module.hpp>
#include <boost/python/def.hpp>
#include <boost/python/lvalue_from_pytype.hpp>

#include <Python.h>
//#define NPY_NO_DEPRECATED_API  NPY_1_7_API_VERSION
#include "numpy/arrayobject.h"
#include "Matrix.h"
#include "Array.h"

namespace CommonTools{

  class PyDeleter{
  public:
  PyDeleter(PyObject *p) : po_(p){
      //std::cout << "PyDeleter CREATE" << std::endl; //XXX DEBUG
      Py_INCREF(po_);
    }
    
    void operator()(void *){
      //std::cout << "PyDeleter DELETE by shared_ptr" << std::endl; //XXX DEBUG
      Py_DECREF(po_);
    }
    
  private:
    PyObject *po_;
  };
  
  template <typename Scalar, int Rows = Dynamic, int Cols = Dynamic>
    class CTpyUtils{
  public:
  static void delete_matrix(PyObject* po){
    //    std::cout << "calling delete_mmatrix" << std::endl; //XXX DEBUG
    Matrix<Scalar,Rows,Cols> * ptr = static_cast<Matrix<Scalar,Rows,Cols>*>(PyCapsule_GetPointer(po, 0));
    delete ptr;
  }
 
  static void delete_array(PyObject* po){
    //    std::cout << "calling delete_mmatrix" << std::endl; //XXX DEBUG
    Array<Scalar,Rows,Cols> * ptr = static_cast<Array<Scalar,Rows,Cols>*>(PyCapsule_GetPointer(po, 0));
    delete ptr;
  }
 
 
  static Matrix<Scalar,Rows,Cols>*
  get_matrix(PyObject *arr,Scalar* data,int rows,int cols){
    if(Rows != Dynamic && Cols != Dynamic)
      return new  Matrix<Scalar,Rows,Cols>(data,PyDeleter(arr));
    else
      return new  Matrix<Scalar,Rows,Cols>(data,Rows==Dynamic?rows:cols,PyDeleter(arr));
   
  }
 
  static Array<Scalar,Rows,Cols>*
  get_array(PyObject *arr,Scalar* data,int rows,int cols){
    if(Rows != Dynamic && Cols != Dynamic)
      return new  Array<Scalar,Rows,Cols>(data,PyDeleter(arr));
    else
      return new  Array<Scalar,Rows,Cols>(data,Rows==Dynamic?rows:cols,PyDeleter(arr));
  }
  };
 
  template <typename Scalar>
    class CTpyUtils<Scalar,Dynamic,Dynamic>{
  public:
    static void delete_matrix(PyObject* po){
      //    std::cout << "calling delete_mmatrix" << std::endl; //XXX DEBUG
      Matrix<Scalar> * ptr = static_cast<Matrix<Scalar>*>(PyCapsule_GetPointer(po, 0));
      delete ptr;
    }
  
    static void delete_array(PyObject* po){
      //    std::cout << "calling delete_mmatrix" << std::endl; //XXX DEBUG
      Array<Scalar> * ptr = static_cast<Array<Scalar>*>(PyCapsule_GetPointer(po, 0));
      delete ptr;
    }
  
    static Matrix<Scalar>*
      get_matrix(PyObject *arr,Scalar* data,int rows,int cols){
      return new  Matrix<Scalar>(data,rows,cols,PyDeleter(arr));
    }
  
    static Array<Scalar>*
      get_array(PyObject *arr,Scalar* data,int rows,int cols){
      return  new Array<Scalar>(data,rows,cols,PyDeleter(arr));
    }
  };
 
  template<typename T>
    class numpy_type_map {
  public:
    static const int typenum;
    static const char * strerr;
  };

  template<>
    const int numpy_type_map<char>::typenum = NPY_BYTE;
  template<>
    const char* numpy_type_map<char>::strerr = "char scalar type expected";
  
  template<>
    const int numpy_type_map<short>::typenum = NPY_SHORT;
  template<>
    const char* numpy_type_map<short>::strerr = "short scalar type expected";


  template<>
    const int numpy_type_map<int>::typenum = NPY_INT;
  template<>
    const char* numpy_type_map<int>::strerr = "int scalar type expected";

  template<>
    const int numpy_type_map<long long>::typenum = NPY_LONGLONG;
  template<>
    const char* numpy_type_map<long long>::strerr = "long long scalar type expected";

  template<>
    const int numpy_type_map<float>::typenum = NPY_FLOAT;
  template<>
    const char* numpy_type_map<float>::strerr = "float scalar type expected";

  template<>
    const int numpy_type_map<double>::typenum = NPY_DOUBLE;
  template<>
    const char* numpy_type_map<double>::strerr = "double scalar type expected";

  template<>
    const int numpy_type_map<bool>::typenum = NPY_BOOL;
  template<>
    const char* numpy_type_map<bool>::strerr = "bool scalar type expected";


  template<>
    const int numpy_type_map<unsigned char>::typenum = NPY_UBYTE;
  template<>
    const char* numpy_type_map<unsigned char>::strerr = "ubyte scalar type expected";


  template<>
    const int numpy_type_map<unsigned short>::typenum = NPY_USHORT;
  template<>
    const char* numpy_type_map<unsigned short>::strerr = "ushort scalar type expected";


  template<>
    const int numpy_type_map<unsigned int>::typenum = NPY_UINT;
  template<>
    const char* numpy_type_map<unsigned int>::strerr = "uint scalar type expected";

}

template<typename Scalar,int Rows = CommonTools::Dynamic, int Cols = CommonTools::Dynamic>
  class Converter{
 public:
 Converter(){}
    
 PyObject* to_numpy(const CommonTools::Matrix<Scalar,Rows,Cols>& m){
   npy_intp shape[2];
   shape[0] = m.rows();
   shape[1] = m.cols();
  
   //std::cout << "creating numpy array("<< shape[0] << "," << shape[1]<<")" << std::endl; //XXX debug
   int npy_type = CommonTools::numpy_type_map<Scalar>::typenum;
  
   //std::cout << "new pyobject:" << (void*) result << std::endl;
   /* object ptr */
   auto capsule = new CommonTools::Matrix<Scalar,Rows,Cols>(m);
  
   //std::cout << "error string " <<  numpy_type_map<T>::strerr << std::endl;//XXX debug
   Scalar * data_ptr = capsule->data();
  
   //std::cout << "data ptr:" << (void*) data_ptr << std::endl;
   PyObject* result = PyArray_SimpleNewFromData(2, shape, npy_type,data_ptr);
  
  
   /* delete function ptr */
   auto destructor = CommonTools::CTpyUtils<Scalar,Rows,Cols>::delete_matrix;
   ((PyArrayObject*) result)->base = PyCapsule_New(capsule, 0, destructor);
  
   //return boost::python::incref(result);
   return result;
 }
  
 CommonTools::Matrix<Scalar,Rows,Cols>* to_matrix(PyObject* arr){
  
   if(PyArray_NDIM(arr) != 2){
     PyErr_SetString(PyExc_ValueError,"argument must be a numpy.ndarray with 2 dimensions.");
     return NULL;
   }
  
   if(PyArray_TYPE(arr) != CommonTools::numpy_type_map<Scalar>::typenum){
     PyErr_SetString(PyExc_ValueError,CommonTools::numpy_type_map<Scalar>::strerr);
     return NULL;
   }
  
  
   PyObject *carr = PyArray_ContiguousFromAny(arr, CommonTools::numpy_type_map<Scalar>::typenum,
					      2, 2);
  
   if (!carr) {
     PyErr_SetString(PyExc_ValueError, "Invalid numpy.ndarray passed.");
     return NULL;   
   }
  
  
   npy_intp * dims = PyArray_DIMS(carr);
  
   return CommonTools::CTpyUtils<Scalar,Rows,Cols>::get_matrix(carr,
							       static_cast<Scalar*>(PyArray_DATA(carr)),
							       dims[0],
							       dims[1]);
 }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/*                                        ARRAY                                                            */
/////////////////////////////////////////////////////////////////////////////////////////////////////////////

  PyObject* to_numpy(const CommonTools::Array<Scalar,Rows,Cols>& m){
  npy_intp shape[2];
  shape[0] = m.rows();
  shape[1] = m.cols();
  
  //std::cout << "creating numpy array("<< shape[0] << "," << shape[1]<<")" << std::endl; //XXX debug
  int npy_type = CommonTools::numpy_type_map<Scalar>::typenum;
  
  //std::cout << "new pyobject:" << (void*) result << std::endl;
  /* object ptr */
  auto capsule = new CommonTools::Array<Scalar,Rows,Cols>(m);
  
  //std::cout << "error string " <<  numpy_type_map<T>::strerr << std::endl;//XXX debug
  Scalar * data_ptr = capsule->data();
  
  //std::cout << "data ptr:" << (void*) data_ptr << std::endl;
  PyObject* result = PyArray_SimpleNewFromData(2, shape, npy_type,data_ptr);
  
  
  /* delete function ptr */
  auto destructor = CommonTools::CTpyUtils<Scalar,Rows,Cols>::delete_array;
  ((PyArrayObject*) result)->base = PyCapsule_New(capsule, 0, destructor);
  
  //return boost::python::incref(result);
  return result;
}
  

  CommonTools::Array<Scalar,Rows,Cols>*
  to_array(PyObject* arr){
  
    if(PyArray_NDIM(arr) != 2){
      PyErr_SetString(PyExc_ValueError,"argument must be a numpy.ndarray with 2 dimensions.");
      return NULL;
    }
    
    if(PyArray_TYPE(arr) != CommonTools::numpy_type_map<Scalar>::typenum){
      PyErr_SetString(PyExc_ValueError,CommonTools::numpy_type_map<Scalar>::strerr);
      return NULL;
    }
    
    
    PyObject *carr = PyArray_ContiguousFromAny(arr, CommonTools::numpy_type_map<Scalar>::typenum,
                                               2, 2);
    
    if (!carr) {
      PyErr_SetString(PyExc_ValueError, "Invalid numpy.ndarray passed.");
      return NULL;   
    }
    
    
    npy_intp * dims = PyArray_DIMS(carr);
  
    return CommonTools::CTpyUtils<Scalar,Rows,Cols>::get_array(carr,
								static_cast<Scalar*>(PyArray_DATA(carr)),
								dims[0],
								dims[1]);
  }
};
