#include "py_eigen.h"
#include "Examples.h"

namespace CommonTools{
  //template<>
//PyTypeObject numpy_type_map<double>::py_TypeObject;
}

BOOST_PYTHON_MODULE(BoostPyEigen){
    using namespace boost::python;
    using namespace CommonTools;

    // initialize type maps for Eigen matrices
    EigenBindingInit();

    class_<DetectorImage, boost::noncopyable>("DetectorImage",init<int,int>())
        .def("get_image", &DetectorImage::get_image,return_value_policy<return_by_value>())
        .def("set_image", &DetectorImage::set_image)
        .def("set_image_const", &DetectorImage::set_image_const)
        .def("get_gain",  &DetectorImage::get_gain)
        .def("set_gain",  &DetectorImage::set_gain)
        .def("get_offset",  &DetectorImage::get_offset)
        .def("set_offset",  &DetectorImage::set_offset)
        .def("zero",  &DetectorImage::zero)
    ;

    class_<VectorExample, boost::noncopyable>("VectorExample",init<int>())
      .def("get",&VectorExample::get)
      .def("get_ref",&VectorExample::get_ref,return_value_policy<return_by_value>())//return_internal_reference<>())
      .def("set",&VectorExample::set)
      .def("set_ref",&VectorExample::set_ref)
      ;


    class_<ArrayExample, boost::noncopyable>("ArrayExample",init<int,int>())
      .def("get",&ArrayExample::get)
      .def("get_ref",&ArrayExample::get_ref,return_value_policy<return_by_value>())//return_internal_reference<>())
      .def("set",&ArrayExample::set)
      .def("set_ref",&ArrayExample::set_ref)
      ;

    
}
