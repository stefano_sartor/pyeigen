#include "Matrix.h"
#include "Array.h"
#include <memory>

class DetectorImage{

public:
    DetectorImage(int rows, int cols):
        image_(rows,cols),
        gain_(0),
        offset_(0){
    }

    CommonTools::Matrix<double>&
    get_image(){
        return image_;
    }

    void
    set_image(CommonTools::Matrix<double> m){
        image_ = m;
    }

    void
    set_image_const(const CommonTools::Matrix<double>& m){
        image_ = m;
    }
    
    double
    get_gain(){
        return gain_;
    }

    void
    set_gain(double g){
        gain_ = g;
    }

    double
    get_offset(){
        return offset_;
    }

    void
    set_offset(double o){
        offset_=o;
    }

   void
    zero(){
        image_.setZero();
    }

private:
    CommonTools::Matrix<double> image_;
    double gain_;
    double offset_;
};


class VectorExample{
  //  typedef CommonTools::Matrix<double,CommonTools::Dynamic,1> Vec;
 public:
  typedef CommonTools::Vector<double> Vec;
  
 VectorExample(int i): v_(i){
  }

  void
  set_ref(Vec& v){v_=v;}

  void
    set(Vec v){v_=v;}
  
  Vec
  get(){return v_;}
  
  Vec&
  get_ref(){return v_;}

private:
  Vec v_;

};



class ArrayExample{
  //  typedef CommonTools::Matrix<double,CommonTools::Dynamic,1> Vec;
 public:
  typedef CommonTools::Array<double> Vec;
  
 ArrayExample(int i, int y): v_(i,y){
  }

  void
  set_ref(Vec& v){v_=v;}

  void
    set(Vec v){v_=v;}
  
  Vec
  get(){return v_;}
  
  Vec&
  get_ref(){return v_;}

private:
  Vec v_;

};
