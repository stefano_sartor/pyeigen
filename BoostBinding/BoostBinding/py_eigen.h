#include <boost/python/class.hpp>
#include <boost/python/module.hpp>
#include <boost/python/def.hpp>
#include <boost/python/lvalue_from_pytype.hpp>

#include <Python.h>
//#define NPY_NO_DEPRECATED_API  NPY_1_7_API_VERSION
#include "numpy/arrayobject.h"
#include "CTEigen/Matrix.h"
#include "CTEigen/Array.h"

namespace CommonTools{

class PyDeleter{
public:
    PyDeleter(PyObject *p) : po_(p){
    //std::cout << "PyDeleter CREATE" << std::endl; //XXX DEBUG
        Py_INCREF(po_);
    }

    void operator()(void *){
      //std::cout << "PyDeleter DELETE by shared_ptr" << std::endl; //XXX DEBUG
        Py_DECREF(po_);
    }

private:
    PyObject *po_;
};

template <typename Scalar, int Rows = Dynamic, int Cols = Dynamic>
class CTpyUtils{
 public:
 static void delete_matrix(PyObject* po){
   //    std::cout << "calling delete_mmatrix" << std::endl; //XXX DEBUG
   Matrix<Scalar,Rows,Cols> * ptr = static_cast<Matrix<Scalar,Rows,Cols>*>(PyCapsule_GetPointer(po, 0));
   delete ptr;
 }
 
 static void delete_array(PyObject* po){
   //    std::cout << "calling delete_mmatrix" << std::endl; //XXX DEBUG
   Array<Scalar,Rows,Cols> * ptr = static_cast<Array<Scalar,Rows,Cols>*>(PyCapsule_GetPointer(po, 0));
   delete ptr;
 }
 
 
 static Matrix<Scalar,Rows,Cols>*
 get_matrix(PyObject *arr,Scalar* data,int rows,int cols,void* ptr){
   if(Rows != Dynamic && Cols != Dynamic)
     return new (ptr) Matrix<Scalar,Rows,Cols>(data,PyDeleter(arr));
   else
     return new (ptr) Matrix<Scalar,Rows,Cols>(data,Rows==Dynamic?rows:cols,PyDeleter(arr));
   
 }
 
 static Array<Scalar,Rows,Cols>*
 get_array(PyObject *arr,Scalar* data,int rows,int cols,void* ptr){
   if(Rows != Dynamic && Cols != Dynamic)
     return new (ptr) Array<Scalar,Rows,Cols>(data,PyDeleter(arr));
   else
     return new (ptr) Array<Scalar,Rows,Cols>(data,Rows==Dynamic?rows:cols,PyDeleter(arr));
 } 
};
 
 template <typename Scalar>
 class CTpyUtils<Scalar,Dynamic,Dynamic>{
 public:
   static void delete_matrix(PyObject* po){
     //    std::cout << "calling delete_mmatrix" << std::endl; //XXX DEBUG
     Matrix<Scalar> * ptr = static_cast<Matrix<Scalar>*>(PyCapsule_GetPointer(po, 0));
     delete ptr;
   }
   
   static void delete_array(PyObject* po){
     //    std::cout << "calling delete_mmatrix" << std::endl; //XXX DEBUG
     Array<Scalar> * ptr = static_cast<Array<Scalar>*>(PyCapsule_GetPointer(po, 0));
     delete ptr;
   }
   
   static Matrix<Scalar>*
     get_matrix(PyObject *arr,Scalar* data,int rows,int cols,void* ptr){
     return new (ptr) Matrix<Scalar>(data,rows,cols,PyDeleter(arr));
   }
   
   static Array<Scalar>*
     get_array(PyObject *arr,Scalar* data,int rows,int cols,void* ptr){
     return new (ptr) Array<Scalar>(data,rows,cols,PyDeleter(arr));
   }
 };
 
template<typename T>
class numpy_type_map {
  public:
      static const int typenum;
      static const char * strerr;
};

template<>
const int numpy_type_map<char>::typenum = NPY_BYTE;
template<>
const char* numpy_type_map<char>::strerr = "char scalar type expected";
  
template<>
const int numpy_type_map<short>::typenum = NPY_SHORT;
template<>
const char* numpy_type_map<short>::strerr = "short scalar type expected";


template<>
const int numpy_type_map<int>::typenum = NPY_INT;
template<>
const char* numpy_type_map<int>::strerr = "int scalar type expected";

template<>
const int numpy_type_map<long long>::typenum = NPY_LONGLONG;
template<>
const char* numpy_type_map<long long>::strerr = "long long scalar type expected";

template<>
const int numpy_type_map<float>::typenum = NPY_FLOAT;
template<>
const char* numpy_type_map<float>::strerr = "float scalar type expected";

template<>
const int numpy_type_map<double>::typenum = NPY_DOUBLE;
template<>
const char* numpy_type_map<double>::strerr = "double scalar type expected";

template<>
const int numpy_type_map<bool>::typenum = NPY_BOOL;
template<>
const char* numpy_type_map<bool>::strerr = "bool scalar type expected";


template<>
const int numpy_type_map<unsigned char>::typenum = NPY_UBYTE;
template<>
const char* numpy_type_map<unsigned char>::strerr = "ubyte scalar type expected";


template<>
const int numpy_type_map<unsigned short>::typenum = NPY_USHORT;
template<>
const char* numpy_type_map<unsigned short>::strerr = "ushort scalar type expected";


template<>
const int numpy_type_map<unsigned int>::typenum = NPY_UINT;
template<>
const char* numpy_type_map<unsigned int>::strerr = "uint scalar type expected";


template<typename Scalar,int Rows = Dynamic, int Cols = Dynamic>
struct CTMatrix2numpy
{
  static PyObject* convert(const CommonTools::Matrix<Scalar,Rows,Cols>& m){
      npy_intp shape[2];
      shape[0] = m.rows();
      shape[1] = m.cols();

      //std::cout << "creating numpy array("<< shape[0] << "," << shape[1]<<")" << std::endl; //XXX debug
      int npy_type = numpy_type_map<Scalar>::typenum;

      //std::cout << "new pyobject:" << (void*) result << std::endl;
      /* object ptr */
      auto capsule = new CommonTools::Matrix<Scalar,Rows,Cols>(m);

      //std::cout << "error string " <<  numpy_type_map<T>::strerr << std::endl;//XXX debug
      Scalar * data_ptr = capsule->data();

      //std::cout << "data ptr:" << (void*) data_ptr << std::endl;
      PyObject* result = PyArray_SimpleNewFromData(2, shape, npy_type,data_ptr);

      
      /* delete function ptr */
      auto destructor = CommonTools::CTpyUtils<Scalar,Rows,Cols>::delete_matrix;
      ((PyArrayObject*) result)->base = PyCapsule_New(capsule, 0, destructor);

       //return boost::python::incref(result);
      return result;
     }
};

template<typename Scalar,int Rows = Dynamic, int Cols = Dynamic>
struct Numpy2CTMatrix
{
  Numpy2CTMatrix()
  {
    boost::python::converter::registry::push_back(
						  &convertible,
						  &construct,
						  boost::python::type_id<CommonTools::Matrix<Scalar,Rows,Cols>>());
  }
  
  // Determine if obj_ptr can be converted in a numpy
  static void* convertible(PyObject* obj_ptr)
  {    if(PyArray_NDIM(obj_ptr) != 2){
      PyErr_SetString(PyExc_ValueError,"argument must be a numpy.ndarray.");
      return NULL;
    }
       
       if(PyArray_TYPE(obj_ptr) != numpy_type_map<Scalar>::typenum){
	 PyErr_SetString(PyExc_ValueError,numpy_type_map<Scalar>::strerr);
	 return NULL;
       }
       return obj_ptr;
  }
  
  static void construct(
			PyObject* obj_ptr,
			boost::python::converter::rvalue_from_python_stage1_data* data)
  {
    
    void* storage = (
		     (boost::python::converter::rvalue_from_python_storage<CommonTools::Matrix<Scalar,Rows,Cols>>*)
		     data)->storage.bytes;
    
    npy_intp * dims = PyArray_DIMS(obj_ptr);
    
    CommonTools::CTpyUtils<Scalar,Rows,Cols>::get_matrix(obj_ptr,static_cast<Scalar*>(PyArray_DATA(obj_ptr)),dims[0],dims[1],storage);
    
    // Stash the memory chunk pointer for later use by boost.python
    data->convertible = storage;
  }
};
 
template<typename Scalar,int Rows = Dynamic, int Cols = Dynamic>
void CTEigen_Matrix_typemap()
{
  using namespace boost::python;

  // register the to-python converter
  to_python_converter<
     CommonTools::Matrix<Scalar,Rows,Cols>,
     CTMatrix2numpy<Scalar,Rows,Cols>>();
  
  // register the from-python converter
  Numpy2CTMatrix<Scalar,Rows,Cols>();
}



template<typename Scalar,int Rows = Dynamic, int Cols = Dynamic>
struct CTArray2numpy
{
  static PyObject* convert(const CommonTools::Array<Scalar,Rows,Cols>& m){
      npy_intp shape[2];
      shape[0] = m.rows();
      shape[1] = m.cols();

      //std::cout << "creating numpy array("<< shape[0] << "," << shape[1]<<")" << std::endl; //XXX debug
      int npy_type = numpy_type_map<Scalar>::typenum;

      //std::cout << "new pyobject:" << (void*) result << std::endl;
      /* object ptr */
      auto capsule = new CommonTools::Array<Scalar,Rows,Cols>(m);

      //std::cout << "error string " <<  numpy_type_map<T>::strerr << std::endl;//XXX debug
      Scalar * data_ptr = capsule->data();

      //std::cout << "data ptr:" << (void*) data_ptr << std::endl;
      PyObject* result = PyArray_SimpleNewFromData(2, shape, npy_type,data_ptr);

      
      /* delete function ptr */
      auto destructor = CommonTools::CTpyUtils<Scalar,Rows,Cols>::delete_array;
      ((PyArrayObject*) result)->base = PyCapsule_New(capsule, 0, destructor);

       //return boost::python::incref(result);
      return result;
     }
};

template<typename Scalar,int Rows = Dynamic, int Cols = Dynamic>
struct Numpy2CTArray
{
  Numpy2CTArray()
  {
    boost::python::converter::registry::push_back(
						  &convertible,
						  &construct,
						  boost::python::type_id<CommonTools::Array<Scalar,Rows,Cols>>());
  }
  
  // Determine if obj_ptr can be converted in a numpy
  static void* convertible(PyObject* obj_ptr)
  {    if(PyArray_NDIM(obj_ptr) != 2){
      PyErr_SetString(PyExc_ValueError,"argument must be a numpy.ndarray.");
      return NULL;
    }
       
       if(PyArray_TYPE(obj_ptr) != numpy_type_map<Scalar>::typenum){
	 PyErr_SetString(PyExc_ValueError,numpy_type_map<Scalar>::strerr);
	 return NULL;
       }
       return obj_ptr;
  }
  
  static void construct(
			PyObject* obj_ptr,
			boost::python::converter::rvalue_from_python_stage1_data* data)
  {
    
    void* storage = (
		     (boost::python::converter::rvalue_from_python_storage<CommonTools::Matrix<Scalar,Rows,Cols>>*)
		     data)->storage.bytes;
    
    npy_intp * dims = PyArray_DIMS(obj_ptr);
    
    CommonTools::CTpyUtils<Scalar,Rows,Cols>::get_array(obj_ptr,static_cast<Scalar*>(PyArray_DATA(obj_ptr)),dims[0],dims[1],storage);
    
    // Stash the memory chunk pointer for later use by boost.python
    data->convertible = storage;
  }
};


template<typename Scalar,int Rows = Dynamic, int Cols = Dynamic>
void CTEigen_Array_typemap()
{
  using namespace boost::python;

  // register the to-python converter
  to_python_converter<
     CommonTools::Array<Scalar,Rows,Cols>,
     CTArray2numpy<Scalar,Rows,Cols>>();
  
  // register the from-python converter
  Numpy2CTArray<Scalar,Rows,Cols>();
}

 
void EigenBindingInit(){
    using namespace boost::python;
    using namespace CommonTools;

    import_array();
    /*Dynamic Matrix typemap instantiation*/
    CTEigen_Matrix_typemap<char,          CommonTools::Dynamic,CommonTools::Dynamic>();
    CTEigen_Matrix_typemap<short,         CommonTools::Dynamic,CommonTools::Dynamic>();  
    CTEigen_Matrix_typemap<int,           CommonTools::Dynamic,CommonTools::Dynamic>();
    CTEigen_Matrix_typemap<float,         CommonTools::Dynamic,CommonTools::Dynamic>();
    CTEigen_Matrix_typemap<double,        CommonTools::Dynamic,CommonTools::Dynamic>();
    CTEigen_Matrix_typemap<long long,     CommonTools::Dynamic,CommonTools::Dynamic>();
    CTEigen_Matrix_typemap<bool,          CommonTools::Dynamic,CommonTools::Dynamic>();
    CTEigen_Matrix_typemap<unsigned char, CommonTools::Dynamic,CommonTools::Dynamic>();
    CTEigen_Matrix_typemap<unsigned short,CommonTools::Dynamic,CommonTools::Dynamic>();
    CTEigen_Matrix_typemap<unsigned int,  CommonTools::Dynamic,CommonTools::Dynamic>();
    
    
    /*Dynamic Vector typemap instantiation*/
    CTEigen_Matrix_typemap<char,          CommonTools::Dynamic,1>();
    CTEigen_Matrix_typemap<short,         CommonTools::Dynamic,1>();
    CTEigen_Matrix_typemap<int,           CommonTools::Dynamic,1>();
    CTEigen_Matrix_typemap<float,         CommonTools::Dynamic,1>();
    CTEigen_Matrix_typemap<double,        CommonTools::Dynamic,1>();
    CTEigen_Matrix_typemap<long long,     CommonTools::Dynamic,1>();
    CTEigen_Matrix_typemap<bool,          CommonTools::Dynamic,1>();
    CTEigen_Matrix_typemap<unsigned char, CommonTools::Dynamic,1>();
    CTEigen_Matrix_typemap<unsigned short,CommonTools::Dynamic,1>();
    CTEigen_Matrix_typemap<unsigned int,  CommonTools::Dynamic,1>();
    
    /*Dynamic RowVector typemap instantiation*/
    CTEigen_Matrix_typemap<char,          1,CommonTools::Dynamic>();
    CTEigen_Matrix_typemap<short,         1,CommonTools::Dynamic>();
    CTEigen_Matrix_typemap<int,           1,CommonTools::Dynamic>();
    CTEigen_Matrix_typemap<float,         1,CommonTools::Dynamic>();
    CTEigen_Matrix_typemap<double,        1,CommonTools::Dynamic>();
    CTEigen_Matrix_typemap<long long,     1,CommonTools::Dynamic>();
    CTEigen_Matrix_typemap<bool,          1,CommonTools::Dynamic>();
    CTEigen_Matrix_typemap<unsigned char, 1,CommonTools::Dynamic>();
    CTEigen_Matrix_typemap<unsigned short,1,CommonTools::Dynamic>();
    CTEigen_Matrix_typemap<unsigned int,  1,CommonTools::Dynamic>();

    /*Dynamic Array typemap instantiation*/
    CTEigen_Array_typemap<char,          CommonTools::Dynamic,CommonTools::Dynamic>();
    CTEigen_Array_typemap<short,         CommonTools::Dynamic,CommonTools::Dynamic>();  
    CTEigen_Array_typemap<int,           CommonTools::Dynamic,CommonTools::Dynamic>();
    CTEigen_Array_typemap<float,         CommonTools::Dynamic,CommonTools::Dynamic>();
    CTEigen_Array_typemap<double,        CommonTools::Dynamic,CommonTools::Dynamic>();
    CTEigen_Array_typemap<long long,     CommonTools::Dynamic,CommonTools::Dynamic>();
    CTEigen_Array_typemap<bool,          CommonTools::Dynamic,CommonTools::Dynamic>();
    CTEigen_Array_typemap<unsigned char, CommonTools::Dynamic,CommonTools::Dynamic>();
    CTEigen_Array_typemap<unsigned short,CommonTools::Dynamic,CommonTools::Dynamic>();
    CTEigen_Array_typemap<unsigned int,  CommonTools::Dynamic,CommonTools::Dynamic>();
    
}
} // namespace CommonTools
