%module pyEigenExample

%{
#include "Examples.h"
%}

%include pyEigen.i


 //include "Examples.h"



class DetectorImage{

public:
    DetectorImage(int rows, int cols);

    CommonTools::Matrix<double>
    get_image();
    
    void
    set_image(CommonTools::Matrix<double> m);

    double
    get_gain();

    void
    set_gain(double g);

    double
    get_offset();

    void
    set_offset(double o);
    
    void zero();

    void print_ptr();

};


class VectorExample{
 public:
  
 VectorExample(int i);

  CommonTools::Vector<double>
  get(){return v_;}

private:
  CommonTools::Vector<double> v_;

};
