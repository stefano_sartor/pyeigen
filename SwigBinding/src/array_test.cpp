#include "CTEigen/Array.h"
#include "CTEigen/Matrix.h"
#include "Matrix.h"
#include <iostream>

using namespace CommonTools;



void test0(){
    /* we specify rows and columns in the constructor */
    Array<double> img(3,3);
    Array<double> img3(3,3);

    Matrix<bool> aa(2,2);

    aa << true, false, true, false;

    Matrix<bool> bb(2,2);

    bb << true, false, true, false;

    Matrix<bool> cc = aa + bb;

    std::cout << "bool array" << std::endl;
    std::cout << cc << std::endl;


    /* populate the matrix using the stream operator together with the comma operator */
    img << 7,8,9,
           4,5,6,
           1,2,3;

    img3 << 7,8,9,
           4,5,6,
           1,2,3;

    /* matrix pretty printed */
    std::cout << img << std::endl;


    std::cout << "mult coeff wise" << std::endl;
    img *= img3;
    std::cout << img << std::endl;

    /* use the call operator to access the elements */
    img(0,0) = 1;
    img(1,1) = 1;
    img(2,2) = 1;

    std::cout << "1 to diagonal" << std::endl;
    std::cout << img << std::endl;


    /* overloaded arithmetic operators allow matrix * scalar operations */
    img = img * 3;

    std::cout << "multiply by scalar 3" << std::endl;
    std::cout << img << std::endl;

    /* new Matrix gets a new buffer with the result */
    Array<double> img2 = img * 2;

    std::cout << "multiply by scalar 2" << std::endl;
    std::cout << img2 << std::endl << std::endl << img << std::endl;
}

void test1(){
   std::cout << "Testing assignment" << std::endl;

    //    Matrix<double> m2(4,4);
    Matrix<double> m3 = Matrix<double>(4,4);
    Matrix<double> m4 = m3;
    m3 = m4;
    std::cout << "Test completed" << std::endl;
}

Matrix<double> test2(){
  return Matrix<double>(4,4);
}

Matrix<double,4,4> test3(){
  return Matrix<double,4,4>();
}

int main(){
  test0();
  test1();
  Matrix<double,4,4> m = Matrix<double,4,4>();
  Matrix<double,4,4> m2 = test3();
  m = m2;
  

  std::cout << "All tests completed" << std::endl;
}
