%{
#include "Python.h"
  //#define NPY_NO_DEPRECATED_API  NPY_1_7_API_VERSION
#include "numpy/arrayobject.h"
#include "CTEigen/Matrix.h"
#include "CTEigen/Array.h"

namespace CommonTools{

class PyDeleter{
public:
    PyDeleter(PyObject *p) : po_(p){
    //std::cout << "PyDeleter CREATE" << std::endl; //XXX DEBUG
        Py_INCREF(po_);
    }
    
    void operator()(void *){
      //std::cout << "PyDeleter DELETE by shared_ptr" << std::endl; //XXX DEBUG
        Py_DECREF(po_);
    }

private:
    PyObject *po_;
};

} // CommonTools
%}

%inline %{

namespace CommonTools{

template <typename Scalar, int Rows = Dynamic, int Cols = Dynamic>
class CTpyUtils{
 public:
 static void delete_matrix(PyObject* po){
   //    std::cout << "calling delete_mmatrix" << std::endl; //XXX DEBUG
   Matrix<Scalar,Rows,Cols> * ptr = static_cast<Matrix<Scalar,Rows,Cols>*>(PyCapsule_GetPointer(po, 0));
   delete ptr;
 }

 static Matrix<Scalar,Rows,Cols>
 get_matrix(PyObject *arr,Scalar* data,int rows,int cols){
   if(Rows != Dynamic && Cols != Dynamic)
     return Matrix<Scalar,Rows,Cols>(data,PyDeleter(arr));
   else
     return Matrix<Scalar,Rows,Cols>(data,Rows==Dynamic?rows:cols,PyDeleter(arr));
     
 }

 
 static Array<Scalar,Rows,Cols>
 get_array(PyObject *arr,Scalar* data,int rows,int cols){
   if(Rows != Dynamic && Cols != Dynamic)
     return Array<Scalar,Rows,Cols>(data,PyDeleter(arr));
   else
     return Array<Scalar,Rows,Cols>(data,Rows==Dynamic?rows:cols,PyDeleter(arr));
 } 
};

template <typename Scalar>
  class CTpyUtils<Scalar,Dynamic,Dynamic>{
 public:
  static void delete_matrix(PyObject* po){
    //    std::cout << "calling delete_mmatrix" << std::endl; //XXX DEBUG
    Matrix<Scalar> * ptr = static_cast<Matrix<Scalar>*>(PyCapsule_GetPointer(po, 0));
    delete ptr;
  }
  
  static void delete_array(PyObject* po){
    //    std::cout << "calling delete_mmatrix" << std::endl; //XXX DEBUG
    Array<Scalar> * ptr = static_cast<Array<Scalar>*>(PyCapsule_GetPointer(po, 0));
    delete ptr;
  }
  static Matrix<Scalar>
    get_matrix(PyObject *arr,Scalar* data,int rows,int cols){
    return Matrix<Scalar>(data,rows,cols,PyDeleter(arr));
  }
  
  static Array<Scalar>
    get_array(PyObject *arr,Scalar* data,int rows,int cols){
    return Array<Scalar>(data,rows,cols,PyDeleter(arr));
  }
}; 

} // namespace CommonTools

    template<typename T>
    class numpy_type_map {
    public:
        static const int typenum;
        static const char * strerr;
    };

    template<>
    const int numpy_type_map<char>::typenum = NPY_BYTE;
    template<>
    const char* numpy_type_map<char>::strerr = "char scalar type expected";

    template<>
    const int numpy_type_map<short>::typenum = NPY_SHORT;
    template<>
    const char* numpy_type_map<short>::strerr = "short scalar type expected";

    template<>
    const int numpy_type_map<int>::typenum = NPY_INT;
    template<>
    const char* numpy_type_map<int>::strerr = "int scalar type expected";

    template<>
    const int numpy_type_map<long long>::typenum = NPY_LONGLONG;
    template<>
    const char* numpy_type_map<long long>::strerr = "long long scalar type expected";

    template<>
    const int numpy_type_map<float>::typenum = NPY_FLOAT;
    template<>
    const char* numpy_type_map<float>::strerr = "float scalar type expected";

    template<>
    const int numpy_type_map<double>::typenum = NPY_DOUBLE;
    template<>
    const char* numpy_type_map<double>::strerr = "double scalar type expected";

    template<>
    const int numpy_type_map<bool>::typenum = NPY_BOOL;
    template<>
    const char* numpy_type_map<bool>::strerr = "bool scalar type expected";

    template<>
    const int numpy_type_map<unsigned char>::typenum = NPY_UBYTE;
    template<>
    const char* numpy_type_map<unsigned char>::strerr = "ubyte scalar type expected";

    template<>
    const int numpy_type_map<unsigned short>::typenum = NPY_USHORT;
    template<>
    const char* numpy_type_map<unsigned short>::strerr = "ushort scalar type expected";

    template<>
    const int numpy_type_map<unsigned int>::typenum = NPY_UINT;
    template<>
    const char* numpy_type_map<unsigned int>::strerr = "uint scalar type expected";
    
    template<>
    const int numpy_type_map<unsigned long>::typenum = NPY_ULONG;
    template<>
    const char* numpy_type_map<unsigned long>::strerr = "ulong scalar type expected";
    
    template<>
    const int numpy_type_map<unsigned long long>::typenum = NPY_ULONGLONG;
    template<>
    const char* numpy_type_map<unsigned long long>::strerr = "ulonglong scalar type expected";        

%}

namespace CommonTools{

%nodefaultctor;
template<typename Scalar, int _Rows = CommonTools::Dynamic, int _Cols = CommonTools::Dynamic>
class Matrix;

template<typename Scalar>
class Vector;

template<typename Scalar>
class RowVector;
  
template<typename Scalar, int _Rows = CommonTools::Dynamic, int _Cols = CommonTools::Dynamic>
class Array;
%clearnodefaultctor;

}


%define CTEigen_Matrix_typemap(Scalar,Rows,Cols)
/*TODO

%typemap(check)

*/
     
%typemap(in) CommonTools::Matrix<Scalar,Rows,Cols> {

    PyObject *arr = $input;

    if(PyArray_NDIM(arr) != 2){
        PyErr_SetString(PyExc_ValueError,"argument must be a numpy.ndarray with 2 dimensions.");
        return NULL;
    }

    if(PyArray_TYPE(arr) != numpy_type_map<Scalar>::typenum){
        PyErr_SetString(PyExc_ValueError,numpy_type_map<Scalar>::strerr);
        return NULL;
    }


    PyObject *carr = PyArray_ContiguousFromAny(arr, numpy_type_map<Scalar>::typenum,
                                               2, 2);
     
    if (!carr) {
        PyErr_SetString(PyExc_ValueError, "Invalid numpy.ndarray passed.");
        return NULL;   
    }
      

    npy_intp * dims = PyArray_DIMS(carr);

    $1 = CommonTools::CTpyUtils<Scalar,Rows,Cols>::get_matrix(
							      carr,
							      static_cast<Scalar*>(PyArray_DATA(carr)),
							      dims[0],
							      dims[1]);
}

%typemap(out) CommonTools::Matrix<Scalar,Rows,Cols> { 
     npy_intp shape[2];
     shape[0] = $1.rows();
     shape[1] = $1.cols();

     int npy_type = numpy_type_map<Scalar>::typenum;

     Scalar * data_ptr = $1.data();

     $result = PyArray_SimpleNewFromData(2, shape, npy_type,data_ptr);
     /* object ptr */
     auto capsule = new CommonTools::Matrix<Scalar,Rows,Cols>($1);
     /* delete function ptr */
     auto destructor = CommonTools::CTpyUtils<Scalar,Rows,Cols>::delete_matrix;
     ((PyArrayObject*) $result)->base = PyCapsule_New(capsule, 0, destructor);
}

%apply CommonTools::Matrix<Scalar,Rows,Cols> { CommonTools::Matrix<Scalar,Rows,Cols>*,  CommonTools::Matrix<Scalar,Rows,Cols>& }; 
%enddef

%define CTEigen_Array_typemap(Scalar,Rows,Cols)
/*TODO

%typemap(check)

*/
%typemap(in) CommonTools::Array<Scalar,Rows,Cols> {
    PyObject *arr = $input;

    
    if(PyArray_NDIM(arr) != 2){
        PyErr_SetString(PyExc_ValueError,"argument must be a numpy.ndarray with 2 dimensions.");
        return NULL;
    }

    if(PyArray_TYPE(arr) != numpy_type_map<Scalar>::typenum){
        PyErr_SetString(PyExc_ValueError,numpy_type_map<Scalar>::strerr);
        return NULL;
    }
    

    PyObject *carr = PyArray_ContiguousFromAny(arr, numpy_type_map<Scalar>::typenum,
    	                                       2, 2);

    if (!carr) {
        PyErr_SetString(PyExc_ValueError, "Invalid numpy.ndarray passed.");
        return NULL;   
    }

    npy_intp * dims = PyArray_DIMS(carr);
 

    $1 = CommonTools::CTpyUtils<Scalar,Rows,Cols>::get_array(
							     carr,
							     static_cast<Scalar*>(PyArray_DATA(carr)),
							     dims[0],
							     dims[1]);
}

%typemap(out) CommonTools::Array<Scalar,Rows,Cols> { 
     npy_intp shape[2];
     shape[0] = ($1)->rows();
     shape[1] = ($1)->cols();

     int npy_type = numpy_type_map<Scalar>::typenum;

     Scalar * data_ptr = ($1)->data();

     $result = PyArray_SimpleNewFromData(2, shape, npy_type,data_ptr);
     /* object ptr */
     auto capsule = new CommonTools::Array<Scalar,Rows,Cols>(*($1));
     /* delete function ptr */
     auto destructor = CommonTools::CTpyUtils<Scalar,Rows,Cols>::delete_array;
     ((PyArrayObject*) $result)->base = PyCapsule_New(capsule, 0, destructor);
}
%apply CommonTools::Array<Scalar,Rows,Cols> { CommonTools::Array<Scalar,Rows,Cols>*,  CommonTools::Array<Scalar,Rows,Cols>& }; 
%enddef

/*Dynamic Matrix typemap instantiation*/
CTEigen_Matrix_typemap(char,          CommonTools::Dynamic,CommonTools::Dynamic)
CTEigen_Matrix_typemap(short,         CommonTools::Dynamic,CommonTools::Dynamic)  
CTEigen_Matrix_typemap(int,           CommonTools::Dynamic,CommonTools::Dynamic)
CTEigen_Matrix_typemap(float,         CommonTools::Dynamic,CommonTools::Dynamic)
CTEigen_Matrix_typemap(double,        CommonTools::Dynamic,CommonTools::Dynamic)
CTEigen_Matrix_typemap(long long,     CommonTools::Dynamic,CommonTools::Dynamic)
CTEigen_Matrix_typemap(bool,          CommonTools::Dynamic,CommonTools::Dynamic)
CTEigen_Matrix_typemap(unsigned char, CommonTools::Dynamic,CommonTools::Dynamic)
CTEigen_Matrix_typemap(unsigned short,CommonTools::Dynamic,CommonTools::Dynamic)
CTEigen_Matrix_typemap(unsigned int,  CommonTools::Dynamic,CommonTools::Dynamic)
CTEigen_Matrix_typemap(unsigned long,  CommonTools::Dynamic,CommonTools::Dynamic)
CTEigen_Matrix_typemap(unsigned long long,  CommonTools::Dynamic,CommonTools::Dynamic)


/*Dynamic Vector typemap instantiation*/
CTEigen_Matrix_typemap(char,          CommonTools::Dynamic,1)
CTEigen_Matrix_typemap(short,         CommonTools::Dynamic,1)  
CTEigen_Matrix_typemap(int,           CommonTools::Dynamic,1)
CTEigen_Matrix_typemap(float,         CommonTools::Dynamic,1)
CTEigen_Matrix_typemap(double,        CommonTools::Dynamic,1)
CTEigen_Matrix_typemap(long long,     CommonTools::Dynamic,1)
CTEigen_Matrix_typemap(bool,          CommonTools::Dynamic,1)
CTEigen_Matrix_typemap(unsigned char, CommonTools::Dynamic,1)
CTEigen_Matrix_typemap(unsigned short,CommonTools::Dynamic,1)
CTEigen_Matrix_typemap(unsigned int,  CommonTools::Dynamic,1)
CTEigen_Matrix_typemap(unsigned long,  CommonTools::Dynamic,1)
CTEigen_Matrix_typemap(unsigned long long,  CommonTools::Dynamic,1)


%apply CommonTools::Matrix<char,           CommonTools::Dynamic, 1> {CommonTools::Vector<char>};
%apply CommonTools::Matrix<short,          CommonTools::Dynamic, 1> {CommonTools::Vector<short>};
%apply CommonTools::Matrix<int,            CommonTools::Dynamic, 1> {CommonTools::Vector<int>};
%apply CommonTools::Matrix<float,          CommonTools::Dynamic, 1> {CommonTools::Vector<float>};
%apply CommonTools::Matrix<double,         CommonTools::Dynamic, 1> {CommonTools::Vector<double>};
%apply CommonTools::Matrix<long long,      CommonTools::Dynamic, 1> {CommonTools::Vector<long long>};
%apply CommonTools::Matrix<bool,           CommonTools::Dynamic, 1> {CommonTools::Vector<bool>};
%apply CommonTools::Matrix<unsigned char,  CommonTools::Dynamic, 1> {CommonTools::Vector<unsigned char>};
%apply CommonTools::Matrix<unsigned short, CommonTools::Dynamic, 1> {CommonTools::Vector<unsigned short>};
%apply CommonTools::Matrix<unsigned int,   CommonTools::Dynamic, 1> {CommonTools::Vector<unsigned int>};
%apply CommonTools::Matrix<unsigned long,   CommonTools::Dynamic, 1> {CommonTools::Vector<unsigned long>};
%apply CommonTools::Matrix<unsigned long long,   CommonTools::Dynamic, 1> {CommonTools::Vector<unsigned long long>};


/*Dynamic RowVector typemap instantiation*/
CTEigen_Matrix_typemap(char,          1,CommonTools::Dynamic)
CTEigen_Matrix_typemap(short,         1,CommonTools::Dynamic)  
CTEigen_Matrix_typemap(int,           1,CommonTools::Dynamic)
CTEigen_Matrix_typemap(float,         1,CommonTools::Dynamic)
CTEigen_Matrix_typemap(double,        1,CommonTools::Dynamic)
CTEigen_Matrix_typemap(long long,     1,CommonTools::Dynamic)
CTEigen_Matrix_typemap(bool,          1,CommonTools::Dynamic)
CTEigen_Matrix_typemap(unsigned char, 1,CommonTools::Dynamic)
CTEigen_Matrix_typemap(unsigned short,1,CommonTools::Dynamic)
CTEigen_Matrix_typemap(unsigned int,  1,CommonTools::Dynamic)
CTEigen_Matrix_typemap(unsigned long,  1,CommonTools::Dynamic)
CTEigen_Matrix_typemap(unsigned long long,  1,CommonTools::Dynamic)


%apply CommonTools::Matrix<char,           1,CommonTools::Dynamic> {CommonTools::RowVector<char>};
%apply CommonTools::Matrix<short,          1,CommonTools::Dynamic> {CommonTools::RowVector<short>};
%apply CommonTools::Matrix<int,            1,CommonTools::Dynamic> {CommonTools::RowVector<int>};
%apply CommonTools::Matrix<float,          1,CommonTools::Dynamic> {CommonTools::RowVector<float>};
%apply CommonTools::Matrix<double,         1,CommonTools::Dynamic> {CommonTools::RowVector<double>};
%apply CommonTools::Matrix<long long,      1,CommonTools::Dynamic> {CommonTools::RowVector<long long>};
%apply CommonTools::Matrix<bool,           1,CommonTools::Dynamic> {CommonTools::RowVector<bool>};
%apply CommonTools::Matrix<unsigned char,  1,CommonTools::Dynamic> {CommonTools::RowVector<unsigned char>};
%apply CommonTools::Matrix<unsigned short, 1,CommonTools::Dynamic> {CommonTools::RowVector<unsigned short>};
%apply CommonTools::Matrix<unsigned int,   1,CommonTools::Dynamic> {CommonTools::RowVector<unsigned int>};
%apply CommonTools::Matrix<unsigned long,   1,CommonTools::Dynamic> {CommonTools::RowVector<unsigned long>};
%apply CommonTools::Matrix<unsigned long long,   1,CommonTools::Dynamic> {CommonTools::RowVector<unsigned long long>};


/*Dynamic Array typemap instantiation*/
CTEigen_Array_typemap(char,          CommonTools::Dynamic,CommonTools::Dynamic)
CTEigen_Array_typemap(short,         CommonTools::Dynamic,CommonTools::Dynamic)  
CTEigen_Array_typemap(int,           CommonTools::Dynamic,CommonTools::Dynamic)
CTEigen_Array_typemap(float,         CommonTools::Dynamic,CommonTools::Dynamic)
CTEigen_Array_typemap(double,        CommonTools::Dynamic,CommonTools::Dynamic)
CTEigen_Array_typemap(long long,     CommonTools::Dynamic,CommonTools::Dynamic)
CTEigen_Array_typemap(bool,          CommonTools::Dynamic,CommonTools::Dynamic)
CTEigen_Array_typemap(unsigned char, CommonTools::Dynamic,CommonTools::Dynamic)
CTEigen_Array_typemap(unsigned short,CommonTools::Dynamic,CommonTools::Dynamic)
CTEigen_Array_typemap(unsigned int,  CommonTools::Dynamic,CommonTools::Dynamic)
CTEigen_Array_typemap(unsigned long,  CommonTools::Dynamic,CommonTools::Dynamic)
CTEigen_Array_typemap(unsigned long long,  CommonTools::Dynamic,CommonTools::Dynamic)


//%typemap(out) CommonTools::Matrix<double,CommonTools::Dynamic,CommonTools::Dynamic> & = CommonTools::Matrix<double,CommonTools::Dynamic,CommonTools::Dynamic>;


%init %{
    import_array();
%}

