#include "Matrix.h"
#include <memory>
#include <iostream>

class DetectorImage{

public:
    DetectorImage(int rows, int cols):
        image_(rows,cols),
        gain_(0),
        offset_(0){
    }

    CommonTools::Matrix<double>
    get_image(){
        return image_;
    }

    void
    set_image(CommonTools::Matrix<double> m){
        image_ = m;
    }

    double
    get_gain(){
        return gain_;
    }

    void
    set_gain(double g){
        gain_ = g;
    }

    double
    get_offset(){
        return offset_;
    }

    void
    set_offset(double o){
        offset_=o;
    }

   void
    zero(){
        image_.setZero();
    }

    void
      print_ptr(){
      std::cout << (void*) image_.data() << std::endl;
    }

    
private:
    CommonTools::Matrix<double> image_;
    double gain_;
    double offset_;
};


class VectorExample{

 public:
  
 VectorExample(int i): v_(i){
  }

  CommonTools::Vector<double>
  get(){return v_;}

private:
  CommonTools::Vector<double> v_;

};

