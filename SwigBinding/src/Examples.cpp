#include "Examples.h"
#include <iostream>

using namespace CommonTools;

int main(){

/*
  Matrix<bool> aa(2,2);

  aa << true, false, true, false;

  Matrix<bool> bb(2,2);

  bb << true, false, true, false;

Matrix<bool> cc = aa + bb;

std::cout << "bool array" << std::endl;
std::cout << cc << std::endl;*/
    /* we specify rows and columns in the constructor */
    Matrix<double> img(3,3);

    /* populate the matrix using the stream operator together with the comma operator */
    img << 7,8,9,
           4,5,6,
           1,2,3;

    /* matrix pretty printed */
    std::cout << img << std::endl;

    DetectorImage d(3,3);

    d.set_image(img);

    /* DetectorImage contains a matrix which shares the img's buffer */
    d.zero();

    std::cout << "img zeroed due to buffer sharing" << std::endl;
    std::cout << img << std::endl;

    /* use the call operator to access the elements */
    img(0,0) = 1;
    img(1,1) = 1;
    img(2,2) = 1;

    /* overloaded arithmetic operators allow matrix * scalar operations */
    img = img * 3;

    /* new Matrix gets a new buffer with the result */
    Matrix<double> img2 = img * 2;
    
    VectorExample v(8);

    auto v1 = v.get();
    
}
